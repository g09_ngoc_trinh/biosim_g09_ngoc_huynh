.. BioSim documentation master file, created by
   sphinx-quickstart on Tue Jan 15 17:40:12 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioSim's documentation!
==================================

.. toctree::
   :caption: Contents:

   simulation
   map
   visualization
   animal
   landscape
   formula

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
