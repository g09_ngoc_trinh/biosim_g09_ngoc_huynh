# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

import pytest
from pytest import approx
from biosim.animal import Animal, Herbivore, Carnivore
from biosim.landscape import Jungle


@pytest.fixture
def set_params(request):
    animal, extra = request.param
    params = {key: animal.get_params()[key] for key in animal.get_params().keys()}
    animal.set_params(extra)
    yield animal
    animal._params = params


def test_init_no_params_animal():
    """Animal with no parameters cannot be initialized as new-born"""
    with pytest.raises(ValueError):
        Animal()


@pytest.mark.parametrize('animal',
                         [Animal, Herbivore, Carnivore])
def test_init_animal(animal):
    animal(1, 10)


@pytest.mark.parametrize(['set_params', 'age', 'weight', 'expected'],
    [
        ((Herbivore, {'a_half': 40, 'phi_age': 0.2, 'w_half': 10, 'phi_weight': 0.1}), 0, 8, 0.45),
        ((Carnivore, {'a_half': 60, 'phi_age': 0.4, 'w_half': 4, 'phi_weight': 0.4}), 0, 8, 0.832),
    ], indirect=['set_params'])
def test_get_fitness(set_params, age, weight, expected):
    animal = set_params(age, weight)
    initial_fitness = animal.fitness
    assert initial_fitness == approx(expected, rel=1e-4, abs=1e-4)
    for _ in range(100):
        animal.aging()
        animal.lose_weight()
    assert initial_fitness > animal.fitness


@pytest.mark.parametrize(['set_params', 'age', 'weight', 'expected'],
    [
        ((Herbivore, {'omega': 0}), 0, 8, False),
        ((Carnivore, {'omega': 0}), 0, 8, False),
        ((Herbivore, {'omega': 1}), 100, 2, True),
        ((Carnivore, {'omega': 1}), 100, 2, True),
    ], indirect=['set_params'])
def test_get_die(set_params, age, weight, expected):
    animal = set_params(age, weight)
    assert animal.die == expected


@pytest.mark.parametrize(['set_params', 'age', 'weight', 'expected'],
    [
        ((Herbivore, {'mu': 0}), 0, 8, False),
        ((Carnivore, {'mu': 0}), 0, 8, False),
        ((Herbivore, {'mu': 1}), 0, 100, True),
        ((Carnivore, {'mu': 1}), 0, 100, True),
    ], indirect=['set_params'])
def test_get_migrate(set_params, age, weight, expected):
    animal = set_params(age, weight)
    assert animal.migrate == expected


@pytest.mark.parametrize(['set_params', 'age', 'weight', 'num', 'expected'],
    [
        ((Herbivore, {'zeta': 1, 'w_birth': 5, 'sigma_birth': 1}), 0, 0, 2, False),
        ((Carnivore, {'zeta': 1, 'w_birth': 5, 'sigma_birth': 1}), 0, 8, 0, False),
        ((Herbivore, {'zeta': 1, 'w_birth': 5, 'sigma_birth': 1}), 0, 8, 1, False),
        ((Carnivore, {'zeta': 1, 'w_birth': 5, 'sigma_birth': 1, 'gamma': 100}), 0, 80, 2, True),
    ], indirect=['set_params'])
def test_birth(set_params, age, weight, num, expected):
    animal = set_params(age, weight)
    assert expected == (animal.birth(num) is not None)


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_aging(species):
    animal = species()
    assert animal.age == 0
    animal.aging()
    assert animal.age == 1


@pytest.mark.parametrize('species', [Herbivore, Carnivore])
def test_lose_weight(species):
    animal = species(0, 8)
    animal.lose_weight()
    assert animal.weight < 8


@pytest.mark.parametrize(['set_params', 'food'],
                         [
                             ((Herbivore, {'F': 3}), Jungle()),
                             ((Carnivore, {'F': 3, 'DeltaPhiMax': -float('inf')}),
                                 [Herbivore(0,3) for _ in range(3)])
                         ], indirect=['set_params'])
def test_eat(set_params, food):
    animal = set_params(0, 50)
    animal.eat(food)
    assert animal.weight > 50
