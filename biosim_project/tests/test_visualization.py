# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


import pytest
import numpy as np
from biosim.visualization import VisualPanel
from biosim.map import Map


def test_init_visual_panel():
    """Test the Visual Panel initialize correct data"""
    visual_panel = VisualPanel(Map('OOO\nOJO\nOOO'),
                               {
                                   'Herbivore': np.array([[0, 0, 0], [0, 20, 0], [0, 0, 0]]),
                                   'Carnivore': np.array([[0, 0, 0], [0, 3, 0], [0, 0, 0]])
                               })
    assert visual_panel.data['Herbivore'] == [20]
    assert visual_panel.data['Carnivore'] == [3]

    herb_ydata = np.full(300, np.nan)
    herb_ydata[0] = 20
    carn_ydata = np.full(300, np.nan)
    carn_ydata[0] = 3

    assert np.allclose(visual_panel.animals_lines['Herbivore'].get_ydata(),
                       herb_ydata, equal_nan=True)
    assert np.allclose(visual_panel.animals_lines['Carnivore'].get_ydata(),
                       carn_ydata, equal_nan=True)


def test_update_visual_panel():
    visual_panel = VisualPanel(Map('OOO\nOJO\nOOO'),
                               {'Herbivore': np.array([[0, 0, 0], [0, 20, 0], [0, 0, 0]]),
                                'Carnivore': np.array([[0, 0, 0], [0, 5, 0], [0, 0, 0]])})

    visual_panel.update_data({'Carnivore': 10, 'Herbivore': 18})
    visual_panel.update_data({'Carnivore': 12, 'Herbivore': 15})
    visual_panel.update_data({'Carnivore': 15, 'Herbivore': 13})
    visual_panel.update_data({'Carnivore': 9, 'Herbivore': 14})

    visual_panel.update({'Herbivore': np.array([[0, 0, 0], [0, 14, 0], [0, 0, 0]]),
                         'Carnivore': np.array([[0, 0, 0], [0, 9, 0], [0, 0, 0]])})

    herb_ydata = np.full(300, np.nan)
    herb_ydata[:5] = [20, 18, 15, 13, 14]
    carn_ydata = np.full(300, np.nan)
    carn_ydata[:5] = [5, 10, 12, 15, 9]

    assert np.allclose(visual_panel.animals_lines['Herbivore'].get_ydata(),
                       list(herb_ydata), equal_nan=True)
    assert np.allclose(visual_panel.animals_lines['Carnivore'].get_ydata(),
                       list(carn_ydata), equal_nan=True)
