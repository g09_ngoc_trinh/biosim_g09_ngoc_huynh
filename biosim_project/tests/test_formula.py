# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

import pytest
from pytest import approx
from biosim.formula import Formula
from scipy.stats import normaltest


@pytest.mark.parametrize('params', [
    [],     # with no arguments
    [1],    # with one argument
    [1, 2]  # with two arguments
])
def test_q_positive_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.q_positive(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 500, 1], 1),         # Test boundary
    ([500, 1, 1], 0),         # Test boundary
    ([0, 40, 0.2], 0.9996),   # A new born
    ([40, 40, 0.2], 0.5),     # A growth-up
    ([50, 40, 0.2], 0.1192),  # Old animal
    ([80, 40, 0.2], 0.0003)   # Old animal
])
def test_q_positive(params, result):
    assert Formula.q_positive(*params) == approx(result, rel=1e-4, abs=1e-4)


@pytest.mark.parametrize('params', [
    [],     # with no arguments
    [1],    # with one argument
    [1, 2]  # with two arguments
])
def test_q_negative_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.q_negative(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 500, 1], 0),           # Test boundary
    ([500, 1, 1], 1),           # Test boundary
    ([1, 10, 0.4], 0.0266),     # A starving animal
    ([10, 10, 0.4], 0.5),       # Half-full animal
    ([30, 10, 0.4], 0.9996),    # Healthy animal
])
def test_q_negative(params, result):
    assert Formula.q_negative(*params) == approx(result, rel=1e-4, abs=1e-4)


@pytest.mark.parametrize('params', [
    [],              # with no arguments
    [1],             # with one argument
    [1, 2],          # with two arguments
    [1, 2, 3],       # with three arguments
    [1, 2, 3, 4],    # with four arguments
    [1, 2, 3, 4, 5]  # with five arguments
])
def test_phi_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.phi(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([500, 1, 1, 0, 500, 1], 0),            # Test boundary
    ([0, 500, 1, 500, 1, 1], 1),            # Test boundary
    ([40, 40, 0.2, 10, 10, 0.4], 0.25),     # A normal animal
    ([0, 40, 0.2, 8, 10, 0.1], 0.45),       # A new-born animal
    ([80, 40, 0.2, 6, 10, 0.1], 0.0001)     # A hungry and old animal
])
def test_phi(params, result):
    assert Formula.phi(*params) == approx(result, rel=1e-4, abs=1e-4)


@pytest.mark.parametrize('params', [
    [],     # with no arguments
    [1],    # with one argument
    [1, 0]  # with two arguments
])
def test_birth_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.birth_prob(*params)


@pytest.mark.parametrize('params', [
    [0.3, -0.1, 1], # phi smaller than 0
    [0.3, 1.1, 1],  # phi larger than 1
])
def test_birth_value_error(params):
    with pytest.raises(ValueError):
        Formula.birth_prob(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 0.5, 1], 0),            # Test boundary
    ([0, 0.5, 20], 0),           # Test boundary
    ([1, 0, 20], 0),             # Test boundary
    ([1, 1, 1], 0),              # Test boundary
    ([1, 1, 0], 0),              # Test boundary
    ([0.2, 1, 20], 1),           # Test boundary
    ([1, 1, 2], 1),              # Test boundary
    ([1, 1, 100], 1),            # Test boundary
    ([0.2, 0.7, 3], 0.28),       # Test real data
])
def test_birth_prob(params, result):
    assert Formula.birth_prob(*params) == approx(result)


@pytest.mark.parametrize('params', [
    [],     # with no arguments
    [1],    # with one argument
])
def test_death_prob_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.death_prob(*params)


@pytest.mark.parametrize('params', [
    [0.4, -0.1],  # phi smaller than 0
    [0.4, 1.1],   # phi larger than 1
])
def test_death_prob_value_error(params):
    with pytest.raises(ValueError):
        Formula.death_prob(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 1], 0),           # Test boundary
    ([0, 0], 1),           # Test boundary
    ([1, 0], 1),           # Test boundary
    ([1, 1], 0),           # Test boundary
    ([1, 0.5], 0.5),       # Test boundary
    ([0.5, 0.5], 0.25),    # Test real data
])
def test_death_prob(params, result):
    assert Formula.death_prob(*params) == approx(result)


@pytest.mark.parametrize('params', [
    [],      # with no arguments
    [1],     # with one argument
    [1, 2],  # with two arguments
])
def test_carn_kill_prob_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.carn_kill_prob(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 1, 1], 0),           # Test boundary
    ([2, 0, 1], 1),           # Test boundary
    ([1, 0.6, 0.5], 0.8),     # Test real data
])
def test_carn_kill_prob(params, result):
    assert Formula.carn_kill_prob(*params) == approx(result)


@pytest.mark.parametrize('params', [
    [],      # with no arguments
    [1],     # with one argument
])
def test_migrate_prob_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.migrate_prob(*params)


@pytest.mark.parametrize('params', [
    [0.2, -0.1],    # phi smaller than 0
    [0.2, 1.1],     # phi larger than 1
])
def test_migrate_prob_error_value(params):
    with pytest.raises(ValueError):
        Formula.migrate_prob(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 1], 0),           # Test boundary
    ([1, 0], 0),           # Test boundary
    ([1, 1], 1),           # Test boundary
    ([0.8, 0.8], 0.64),    # Test real data
])
def test_migrate_prob(params, result):
    assert Formula.migrate_prob(*params) == approx(result)


@pytest.mark.parametrize('params', [
    [],      # with no arguments
    [1],     # with one argument
])
def test_gain_weight_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.gain_weight(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 0], 0),           # Test boundary
    ([0.8, 0], 0),         # Test boundary
    ([0.8, 1], 0.8),       # Test real data
])
def test_gain_weight(params, result):
    assert Formula.gain_weight(*params) == approx(result)


@pytest.mark.parametrize('params', [
    [],      # with no arguments
    [1],     # with one argument
])
def test_lose_weight_invalid_param(params):
    with pytest.raises(TypeError):
        Formula.lose_weight(*params)


@pytest.mark.parametrize(['params', 'result'], [
    ([0, 1], 0),           # Test boundary
    ([1, 0], 0),           # Test boundary
    ([0.5, 0.3], 0.15),    # Test real data
])
def test_lose_weight(params, result):
    assert Formula.lose_weight(*params) == approx(result)


def test_birth_weight():
    actual = [Formula.birth_weight(8, 1.5) for _ in range(1000)]
    alpha = 1e-3
    k2, p = normaltest(actual)
    assert p > alpha

