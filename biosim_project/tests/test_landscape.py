# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

import pytest
from biosim.landscape import Landscape, Jungle, Savannah


@pytest.mark.parametrize('lscape',
                         [Landscape, Jungle, Savannah])
def test_init(lscape):
    lscape()


@pytest.fixture
def set_params(request):
    lscape, extra = request.param
    params = {key: lscape.get_params()[key] for key in lscape.get_params().keys()}
    lscape.set_params(extra)
    yield lscape
    lscape._params = params

@pytest.mark.parametrize(['set_params', 'f', 'result', 'remain'],
                         [
                             ((Landscape, {'f_max': 10}), 10, 10, 0),
                             ((Landscape, {'f_max': 10}), 5, 5, 5),
                             ((Landscape, {'f_max': 10}), 11, 10, 0)
                         ], indirect=['set_params'])
def test_eaten(set_params, f, result, remain):
    lscape = set_params()
    assert lscape.eaten(f) == result
    assert lscape._fodder == remain


@pytest.mark.parametrize(['set_params', 'f', 'result'],
                         [
                             ((Jungle, {'f_max': 10}), 10, 10),
                             ((Jungle, {'f_max': 10}), 5, 10),
                             ((Jungle, {'f_max': 10}), 11, 10),
                             ((Jungle, {'f_max': 10}), 0, 10),
                             ((Savannah, {'f_max': 10, 'alpha': 0.5}), 10, 5),
                             ((Savannah, {'f_max': 10, 'alpha': 0.5}), 5, 7.5),
                             ((Savannah, {'f_max': 10, 'alpha': 0.5}), 0, 10),
                             ((Savannah, {'f_max': 10, 'alpha': 1}), 10, 10),
                             ((Savannah, {'f_max': 10, 'alpha': 0}), 10, 0)
                         ], indirect=['set_params'])
def test_grow(set_params, f, result):
    lscape = set_params()
    lscape.eaten(f)
    lscape.grow()
    assert lscape._fodder == result
