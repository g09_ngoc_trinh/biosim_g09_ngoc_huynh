# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


from biosim.simulation import BioSim
from biosim.animal import Herbivore
from biosim.landscape import Jungle


def test_num_animals_per_species():
    """
    Test if property num_animals_per_species works as expected
    """
    biosim = BioSim(island_map='OOOO\nOJJO\nOJJO\nOOOO', ini_pop=[], seed=88894)
    biosim.add_population([
        {'loc': (2,2), 'pop':[{'species': 'Herbivore', 'weight': 40, 'age': 4} for _ in range(100)]}
    ])
    biosim.simulate(100, 1000, 1000)
    expected = {key: 0 for key in biosim._species_name.keys()}
    for row in biosim.population:
        for cell in row:
            for key in cell.keys():
                expected[key] += len(cell[key])

    assert expected == biosim.num_animals_per_species
    biosim.add_population([
        {'loc': (2, 2),
         'pop': [{'species': 'Carnivore', 'weight': 40, 'age': 4} for _ in range(5)]}
    ])

    biosim.simulate(100, 1000, 1000)
    expected = {key: 0 for key in biosim._species_name.keys()}
    for row in biosim.population:
        for cell in row:
            for key in cell.keys():
                expected[key] += len(cell[key])

    assert expected == biosim.num_animals_per_species


def test_num_animals_consistency():
    """
    When no lost or addition of animals, the total number of animal should maintain
    """
    biosim = BioSim(island_map='OOOO\nOJJO\nOJJO\nOOOO', ini_pop=[], seed=88894)
    biosim.set_animal_parameters('Herbivore', {
        'omega': 0,
        'gamma': 0,
        'mu': 1,
        'a_half': 1000
    })

    biosim.set_animal_parameters('Carnivore', {
        'omega': 0,
        'gamma': 0,
        'mu': 1,
        'a_half': 1000,
        'F': 0
    })
    biosim.add_population([
        {'loc': (2,2), 'pop':[{'species': 'Herbivore', 'weight': 40, 'age': 4} for _ in range(40)]}
    ])

    biosim.simulate(100, 200)
    assert biosim.num_animals == 40
    assert biosim.num_animals_per_species == {'Herbivore': 40, 'Carnivore': 0}

    biosim.add_population([
        {'loc': (2, 2),
         'pop': [{'species': 'Carnivore', 'weight': 40, 'age': 4} for _ in range(40)]}
    ])
    biosim.simulate(100, 200)
    assert biosim.num_animals == 80
    assert biosim.num_animals_per_species == {'Herbivore': 40, 'Carnivore': 40}


def test_biosim_cycle_feeding():
    """New fodder grows every year"""

    default_f_max = Jungle.get_params()['f_max']
    default_F = Herbivore.get_params()['F']
    default_beta = Herbivore.get_params()['beta']
    biosim = BioSim(island_map="OOO\nOJO\nOOO",
                    ini_pop=[],
                    seed=889888)

    biosim.add_population([
        {'loc': (2, 2),
         'pop': [{'species': 'Herbivore', 'weight': 40, 'age': 4}]}
    ])
    biosim.set_animal_parameters('Herbivore', {
        'F': 40,
        'beta': 1
    })

    biosim.set_landscape_parameters('J', {'f_max': 800})

    for _ in range(5):
        biosim.simulate(1, 1000, 1000)
        assert biosim.island_map[1, 1]._fodder == 760

    biosim.set_animal_parameters('Herbivore', {
        'F': default_F,
        'beta': default_beta
    })
    biosim.set_landscape_parameters('J', {'f_max': default_f_max})


def test_biosim_cycle_aging():
    """Animals get older every year"""
    biosim = BioSim(island_map="OOO\nOJO\nOOO",
                    ini_pop=[],
                    seed=889888)

    default_omega = Herbivore.get_params()['omega']
    biosim.set_animal_parameters('Herbivore', {'omega': 0})
    biosim.add_population([
        {'loc': (2, 2),
         'pop': [{'species': 'Herbivore', 'weight': 40, 'age': 4} for _ in range(10)]}
    ])

    assert [h.age for h in biosim.population[1, 1]['Herbivore']] == [4 for _ in range(10)]

    for age in range(5, 10):
        biosim.simulate(1, 100, 100)
        print([h.fitness for h in biosim.population[1, 1]['Herbivore']])
        assert [h.age for h in biosim.population[1, 1]['Herbivore']] == [age for _ in range(10)]

    biosim.set_animal_parameters('Herbivore', {'omega': default_omega})
