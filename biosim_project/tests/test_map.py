# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


import pytest
from biosim.map import Map
from biosim.map import Desert


def test_empty_island():
    """Empty island can be created"""
    Map(island_map="OO\nOO")


def test_single_cell_island():
    """Island with a single jungle cell"""
    Map(island_map="OOO\nOJO\nOOO")


def test_all_landscape():
    """Island with all types of landscape"""
    Map(island_map="OOOO\nOJSO\nOMDO\nOOOO")


@pytest.mark.parametrize('bad_boundary',
                         ['J', 'S', 'M', 'D'])
def test_invalid_boundary(bad_boundary):
    """Value error raised when no ocean boundary in map"""
    with pytest.raises(ValueError):
        Map(island_map="{}OO\nOJO\nOOO".format(bad_boundary))


def test_invalid_landscape():
    """Island with invalid landscape"""
    with pytest.raises(ValueError):
        Map(island_map="OOO\nOLO\nOOO")


def test_inconsistent_length():
    """Island with non-rectangular shape"""
    with pytest.raises(ValueError):
        Map(island_map="OOO\nOJJO\nOOO")


@pytest.mark.parametrize(['coord', 'name'],
                         [
                             ((1, 1), 'Jungle'),
                             ((1, 2), 'Savannah'),
                             ((2, 1), 'Mountain'),
                             ((2, 2), 'Desert'),
                             ((0, 0), 'Ocean')
                         ])
def test_get_landscape(coord, name):
    """Test island_map[x, y] should return the landscape at (x, y) coord"""
    island_map = Map(island_map="OOOO\nOJSO\nOMDO\nOOOO")
    assert type(island_map[coord]).__name__ == name


def test_set_landscape():
    """Test change map landscape using coord"""
    island_map = Map(island_map="OOOO\nOJSO\nOMDO\nOOOO")
    assert type(island_map[1, 1]).__name__ == 'Jungle'
    island_map[1, 1] = Desert()
    assert type(island_map[1, 1]).__name__ == 'Desert'


def test_get_color_map():
    """Test the color map of the island"""
    color_map = Map(island_map="OOOO\nOJSO\nOMDO\nOOOO").color_map
    for row in color_map:
        for cell in row:
            assert len(cell) == 3


def test_iterate_map():
    """Test iterate inside island map"""
    island_map = Map(island_map="OOOO\nOJSO\nOMDO\nOOOO")
    for row in island_map:
        for cell in row:
            cell.set_params({})
