# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


import matplotlib.pyplot as plt
import numpy as np


class VisualPanel:
    """ The Visual Panel is the Main GUI for the simulator to display the results """
    _default_cmax = 200
    _default_ymax = 300
    _num_display_year = 300
    _color_map = 'jet'
    _plt_pause_time = 1e-6

    def __init__(self, island_map, distribution, title='BioSim',
                 map_title='Island',
                 animal_title='Animal Count',
                 cmax_animals=None,
                 ymax_animals=None
                 ):
        """
        Initialize the visualization graphic

        :param island_map: biosim.map Map instance
        :param distribution: Dict specifying initial value of each species distribution
        :param title: Figure title
        :param map_title: title of the island map
        :param animal_title: title of the animal-per-year graph
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
                             If not specified, then it will adjust automatically
        """
        self._figure = plt.figure()
        self._figure.suptitle(title)

        num_species = len(distribution.keys())
        plot_shape = (2, max(2, num_species))

        # Initialize the landscape map
        self.island_map = plt.subplot2grid(plot_shape, (0, 0))

        map_xticks = range(island_map.shape[1])
        map_yticks = range(island_map.shape[0])
        map_xticklabels = [x if x == 1 or x % 5 == 0 else ''
                           for x in range(1, 1 + island_map.shape[1])]
        map_yticklabels = [y if y == 1 or y % 5 == 0 else ''
                           for y in range(1, 1 + island_map.shape[0])]

        self.island_map.set_title(map_title)
        self.island_map.set_xticks(map_xticks)
        self.island_map.set_yticks(map_yticks)
        self.island_map.set_xticklabels(map_xticklabels)
        self.island_map.set_yticklabels(map_yticklabels)
        self.island_map.imshow(island_map.color_map)

        # Initialize animals number per year graph
        animal_per_year = plt.subplot2grid(plot_shape, (0, 1), colspan=max(1, num_species - 1))

        year_data = np.arange(self._num_display_year)

        animal_per_year.set_title(animal_title)
        animal_per_year.set_xlim(0, self._num_display_year)

        # if ymax_animals is not set, auto height for animal count graph is based on
        # self._current_ymax
        self._current_ymax = None
        y_max = ymax_animals
        if y_max is None:
            y_max = self._default_ymax
            self._current_ymax = self._default_ymax

        animal_per_year.set_ylim(0, y_max)

        data = {}
        animal_lines = {}
        for key in distribution.keys():
            num_animal = np.sum(distribution[key])
            data[key] = [num_animal]
            y_data = np.full(self._num_display_year, np.nan)
            y_data[0] = num_animal
            animal_lines[key] = animal_per_year.plot(year_data, y_data, label=key)[0]

        plt.legend()

        self.data = data
        self.animals_ax = animal_per_year
        self.animals_lines = animal_lines

        # Initialize distribution maps
        if cmax_animals:
            self.cmax = cmax_animals
        else:
            self.cmax = self._default_cmax

        distribution_axis = {}
        color_bars = {}
        distribution_maps = {}
        col_idx = 0
        for key in distribution.keys():
            distribution_ax = plt.subplot2grid(plot_shape, (1, col_idx))
            distribution_ax.set_title('{} Distribution'.format(key))

            distribution_ax.set_xticks(map_xticks)
            distribution_ax.set_yticks(map_yticks)
            distribution_ax.set_xticklabels(map_xticklabels)
            distribution_ax.set_yticklabels(map_yticklabels)

            if cmax_animals and key in cmax_animals.keys():
                cmax = cmax_animals[key]
            else:
                cmax = self._default_cmax

            distribution_maps[key] = distribution_ax.imshow(distribution[key],
                                                            cmap=self._color_map, vmin=0, vmax=cmax)

            distribution_axis[key] = distribution_ax
            color_bars[key] = self._figure.colorbar(distribution_maps[key], ax=distribution_ax)
            col_idx += 1

        self.distribution_axis = distribution_axis
        self.distribution_maps = distribution_maps
        self.color_bars = color_bars

        # Initialize year indicator
        self._year = 0
        self._year_ax = self._figure.add_axes([0.85, 0.95, 0.15, 0.05])
        self._year_ax.set_axis_off()
        self._year_txt = self._year_ax.text(0, 0, 'Year: {}'.format(self._year))

    def update_data(self, num_animals_per_species):
        """
        Update the number of animals per species every year

        :param num_animals_per_species: dict specifying number of animal per species in a year
        """
        for key in num_animals_per_species.keys():
            self.data[key].append(num_animals_per_species[key])

        y_max = max(num_animals_per_species.values())

        if self._current_ymax:
            if self._current_ymax < y_max * 1.1:
                self._current_ymax = int(y_max * 1.1)

        self._year += 1

    def update(self, distribution):
        """
        Update the Visual Panel using new distribution

        :param distribution: dict specifying distribution of animal per species in the updated year
        """

        self.animals_ax.set_ylim(0, self._current_ymax)
        self._year_txt.set_text('Year: {}'.format(self._year))

        num_display_year = self._num_display_year
        num_year = len(list(self.data.values())[0])

        if num_year > num_display_year:
            self.animals_ax.set_xlim(num_year - num_display_year, num_year)
            for key in self.data.keys():
                self.animals_lines[key].set_xdata(np.arange(0, num_year)[-num_display_year:])
                self.animals_lines[key].set_ydata(self.data[key][-num_display_year:])
        else:
            for key in self.data.keys():
                y_data = self.animals_lines[key].get_ydata()
                y_data[:num_year] = self.data[key]
                self.animals_lines[key].set_ydata(y_data)

        for key in distribution.keys():
            self.distribution_maps[key].set_data(distribution[key])

        plt.pause(self._plt_pause_time)

    @property
    def figure(self):
        """ Return the figure object of the Visual Panel. Used for customize GUI"""
        return self._figure
