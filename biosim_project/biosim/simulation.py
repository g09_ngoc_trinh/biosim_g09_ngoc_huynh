# -*- coding: utf-8 -*-

"""
The BioSim class handles everything that happens in simulation
"""

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

from .map import Map
from .animal import Herbivore, Carnivore
from .visualization import VisualPanel
import numpy as np
import random as rd
import pandas as pd
from operator import attrgetter
import warnings
import subprocess


_FFMPEG_BINARY = 'ffmpeg'


class BioSim:
    """
    The main simulator of the biosim
    """
    _species_name = {
        'Herbivore': Herbivore,
        'Carnivore': Carnivore
    }

    _default_movie_fmt = 'mp4'

    def __init__(self, island_map, ini_pop, seed,
                 ymax_animals=None, cmax_animals=None,
                 img_base=None, img_fmt='png'):
        """
        Initialize a simulator with landscape map and animal distribution

        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param img_base: String with beginning of file name for figures, including path
        :param img_fmt: String with file type for figures, e.g. 'png'

        If ymax_animals is None, the y-axis limit should be adjusted automatically.

        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
           {'Herbivore': 50, 'Carnivore': 20}

        If img_base is None, no figures are written to file.
        Filenames are formed as

            '{}_{:05d}.{}'.format(img_base, img_no, img_fmt)

        where img_no are consecutive image numbers starting from 0.
        img_base should contain a path and beginning of a file name.
        """
        rd.seed(seed)

        # First initialize hidden attributes for plotting and data storage
        self._num_animals_per_species = {key: 0 for key in self._species_name.keys()}
        self._year = 0
        self._visual_panel = None

        self._ymax_animals = ymax_animals
        self._cmax_animals = cmax_animals

        self._img_no = 0

        # Next initialize map and population
        self.island_map = Map(island_map)
        self.population = None
        self.add_population(ini_pop)

        # Initialize stopped flag for simulation
        self._stopped = False

        # Finally set data for make movies
        self.img_base = img_base
        self.img_fmt = img_fmt

        if self.img_base is None:
            warnings.warn('No filename defined. No images will be saved while simulation',
                          UserWarning)

    def set_animal_parameters(self, species, params):
        """
        Set parameters for animal species.

        :param species: String, name of animal species
        :param params: Dict with valid parameter specification for species
        """
        self._species_name[species].set_params(params)

    def set_landscape_parameters(self, landscape, params):
        """
        Set parameters for landscape type.

        :param landscape: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """
        self.island_map.set_params(landscape, params)

    def simulate(self, num_years, vis_years=1, img_years=None):
        """
        Run simulation while visualizing the result.

        :param num_years: number of years to simulate
        :param vis_years: years between visualization updates
        :param img_years: years between visualizations saved to files (default: vis_years)

        Image files will be numbered consecutively.
        """
        self._stopped = False

        if img_years is None:
            img_yrs = vis_years
        else:
            img_yrs = img_years

        # The initial map and graph should be shown
        self._update_visual_panel()
        self._save_img()

        # Start simulate
        for _ in range(num_years):
            self.simulate_one_year()
            self._year += 1

            self._update_data()
            if self._year % vis_years == 0:
                self._update_visual_panel()
            if self._year % img_yrs == 0:
                self._save_img()

            if self._stopped:
                break

    def pause(self):
        """Pause the current simulation"""
        self._stopped = True

    def simulate_one_year(self):
        """Simulate an annual cycle"""
        shape = self.island_map.shape
        for i in range(shape[0]):
            for j in range(shape[1]):
                current_landscape = self.island_map[i, j]
                pop = self.population[i, j]

                self._feeding(pop, current_landscape)
                self._procreation(pop)
                self._migrate((i, j), pop)

        for row in self.population:
            for pop in row:
                self._age_and_die(pop)

    def add_population(self, population):
        """
        Add a population to the island

        :param population: List of dictionaries specifying population

        [{'loc': (3,4),
          'pop': [{'species': 'Herbivore', 'age': 10, 'weight': 15},
                  {'species': 'Herbivore', 'age': 5, 'weight': 40},
                  {'species': 'Herbivore', 'age': 15, 'weight': 25}]},
         {'loc': (4,4),
          'pop': [{'species': 'Herbivore', 'age': 2, 'weight': 60},
                  {'species': 'Herbivore', 'age': 9, 'weight': 30},
                  {'species': 'Herbivore', 'age': 16, 'weight': 14}]},
         {'loc': (4,4),
          'pop': [{'species': 'Carnivore', 'age': 3, 'weight': 35},
                  {'species': 'Carnivore', 'age': 5, 'weight': 20},
                  {'species': 'Carnivore', 'age': 8, 'weight': 5}]}]

        """
        if self.population is None:
            # Initialize population map
            self.population = np.empty(self.island_map.shape, dtype=object)
            for i in range(self.population.shape[0]):
                for j in range(self.population.shape[1]):
                    self.population[i, j] = {key: [] for key in self._species_name.keys()}

        for pop in population:
            coord = (pop['loc'][0] - 1, pop['loc'][1] - 1)
            p = self.population[coord]

            for animal in pop['pop']:
                species = animal['species']
                age = animal['age']
                weight = animal['weight']
                p[species].append(self._species_name[species](age, weight))
                self._num_animals_per_species[species] += 1

    def make_movie(self):
        """Create MPEG4 movie from visualization images saved."""
        if self.img_base is None:
            raise RuntimeError("No filename defined.")
        try:
            # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
            # section "Compatibility"
            subprocess.check_call([_FFMPEG_BINARY,
                                   '-i', '{}_%05d.png'.format(self.img_base),
                                   '-y',
                                   '-profile:v', 'baseline',
                                   '-level', '3.0',
                                   '-pix_fmt', 'yuv420p',
                                   '{}.{}'.format(self.img_base,
                                                  self._default_movie_fmt)])
        except subprocess.CalledProcessError as err:
            raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))

    @property
    def year(self):
        """Last year simulated."""
        return self._year

    @property
    def num_animals(self):
        """Total number of animals on island."""
        res = 0
        for val in self._num_animals_per_species.values():
            res += val
        return res

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""
        return self._num_animals_per_species

    @property
    def animal_distribution(self):
        """Pandas DataFrame with animal count per species for each cell on island."""
        population = self.population
        shape = population.shape

        data = [[i + 1, j + 1,
                 len(population[i, j]['Herbivore']),
                 len(population[i, j]['Carnivore'])]
                for i in range(shape[0]) for j in range(shape[1])]

        return pd.DataFrame(data, columns=['Row', 'Col', 'Herbivore', 'Carnivore'])

    @property
    def visual_panel(self):
        """ Return the Visual Panel of the simulation. Used for customize GUI"""
        if self._visual_panel is None:
            self._init_visual_panel()

        return self._visual_panel

    def _update_data(self):
        """ update the current distribution map of animals"""
        num_animals_per_species = self.num_animals_per_species
        self._visual_panel.update_data(num_animals_per_species)

    def _init_visual_panel(self):
        """
        The figure and graph should initialize firstly here
        """
        self._visual_panel = VisualPanel(self.island_map, self._get_animal_maps(),
                                         cmax_animals=self._cmax_animals,
                                         ymax_animals=self._ymax_animals)

    def _update_visual_panel(self):
        """
        The graph and map are updated base on data changed
        """
        if self._visual_panel is None:
            self._init_visual_panel()

        self._visual_panel.update(self._get_animal_maps())

    def _get_animal_maps(self):
        """ Return Dict specifying the current animal distribution per species"""
        return {key: [[len(cell[key]) for cell in row] for row in self.population]
                for key in self._species_name.keys()}

    def _feeding(self, pop, landscape):
        """
        Feeding of all animals in the population at the current landscape

        :param pop: one-cell population
        :param landscape: the landscape at that cell
        """
        # First the landscape grows back fodder
        if landscape.get_params()['f_max'] > 0:
            landscape.grow()

        # Then the herbivores eat fodders, from the healthiest one
        herbs = pop['Herbivore']
        carns = pop['Carnivore']

        sorted(herbs, key=attrgetter('fitness'), reverse=True)
        for h in herbs:
            h.eat(landscape)

        # Finally the carnivores from the healthiest hunt for herbivore, started from the
        # herbivore with lowest fitness
        sorted(carns, key=attrgetter('fitness'), reverse=True)
        sorted(herbs, key=attrgetter('fitness'))
        for c in carns:
            herbs = c.eat(herbs, is_sorted=True)

        self._num_animals_per_species['Herbivore'] -= len(pop['Herbivore']) - len(herbs)
        pop['Herbivore'] = herbs

    def _procreation(self, pop):
        """
        A population of animal can have offsprings every year

        :param pop: animal population at one cell
        """
        for key in self._species_name.keys():
            new_born = []
            animals = pop[key]
            num_animal = len(animals)
            for animal in animals:
                new_baby = animal.birth(num_animal)
                if new_baby is not None:
                    new_born.append(new_baby)
            if new_born:
                animals.extend(new_born)
                self._num_animals_per_species[key] += len(new_born)

    def _migrate(self, coord, pop):
        """
        Every animal in an population has chance to migrate every year

        :param pop: animal population at one cell
        """
        for species in self._species_name.keys():
            animals = pop[species]
            migrate_animals = []
            for idx, animal in enumerate(animals):
                if animal.migrate:
                    direction = rd.randint(1, 4)
                    shape = self.population.shape
                    new_coord = None
                    if direction == 1 and coord[0] > 0:
                        new_coord = (coord[0] - 1, coord[1])
                    elif direction == 2 and coord[0] < shape[0]:
                        new_coord = (coord[0] + 1, coord[1])
                    elif direction == 3 and coord[1] > 0:
                        new_coord = (coord[0], coord[1] - 1)
                    elif direction == 4 and coord[1] < shape[1]:
                        new_coord = (coord[0], coord[1] + 1)

                    if new_coord is not None:
                        new_landscape = self.island_map[new_coord]

                        # Animals only migrate to landscape with fodder
                        if new_landscape.get_params()['f_max'] > 0:
                            migrate_animals.append(animal)
                            self.population[new_coord][species].append(animal)
                            animals[idx] = None

            pop[species] = [animal for animal in animals if animal is not None]

    def _age_and_die(self, pop):
        """
        every animal in the population loses its weight and gets older every year

        :param pop: animal population at one cell
        """
        for species in self._species_name.keys():
            animals = pop[species]
            for animal in animals:
                animal.aging()
                animal.lose_weight()

            pop[species] = [animal for animal in animals if not animal.die]
            self._num_animals_per_species[species] -= len(animals) - len(pop[species])

    def _save_img(self):
        """ Save images to make movie """
        if self.img_base is None:
            return
        fig = self._visual_panel.figure
        fig.savefig('{}_{:05d}.{}'.format(self.img_base, self._img_no, self.img_fmt))
        self._img_no += 1
