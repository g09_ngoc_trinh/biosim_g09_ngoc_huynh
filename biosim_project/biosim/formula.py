# -*- coding: utf-8 -*-

"""
The Formula class define and calculate value inside biosim based on predefine function
"""

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

import math as math
import random as rd
import numba


class Formula:
    """ A class contains static methods for calculation of animal status"""
    @staticmethod
    @numba.jit(nopython=True)
    def q_positive(x, x_half, phi):
        """
        q_positive is calculated based on the following formula.

        `q_positive(x, x_half, phi) = 1 / (1 + e^(phi * (x - x_half)))`

        :param x:
        :param x_half:
        :param phi:
        :return: q_positve value
        """
        return 1 / (1 + math.exp(phi * (x - x_half)))

    @staticmethod
    @numba.jit(nopython=True)
    def q_negative(x, x_half, phi):
        """
        q_negative is calculated based on the following formula

        `q_negative(x, x_half, phi) = 1 / (1 + e^-(phi * (x - x_half)))`

        :param x:
        :param x_half:
        :param phi:
        :return: q_negative value
        """
        return 1 / (1 + math.exp(phi * (x_half - x)))

    @staticmethod
    def phi(age, a_half, phi_age, weight, w_half, phi_weight):
        """
        Finess / phi is 0 when weight is 0, else it is calculated based on the formula:

        `phi = q_positive(a, a_half, phi_age) + q_negative(w, w_half, phi_weight)`

        :param age: age of the animal
        :param a_half: a_half param of the species
        :param phi_age: phi_age param of the species
        :param weight: weight of the animal
        :param w_half: w_half param of the species
        :param phi_weight: phi_weight param of the species
        :return: the calculated finess phi, where 0 <= phi <=1
        """
        if weight <= 0:
            return 0

        return Formula.q_positive(age, a_half, phi_age) * Formula.q_negative(weight, w_half,
                                                                             phi_weight)

    @staticmethod
    def birth_prob(gamma, phi, N):
        """
        Find the probability to generate offspring. When N < 2 the probability is 0, else:

        `birth = min(1, gamma * phi * (N - 1)`

        :param gamma: gamma param of the species
        :param phi: current fitness of the animal
        :param N: number of the same kind of species in an area
        :return: the probability to generate offspring
        """
        if phi > 1 or phi < 0:
            raise ValueError('Phi must be in range [0,1]')
        if N < 2:
            return 0

        return min(1, gamma * phi * (N - 1))

    @staticmethod
    def death_prob(omega, phi):
        """
        Find the probability for an animal to die. When phi = 0 then the probability is 100%, else:

        `death = omega * (1 - phi)`

        :param omega: omega param of the species
        :param phi: current fitness of the animal
        :return: the probability to die
        """
        if phi > 1 or phi < 0:
            raise ValueError('Phi must be in range[0,1]')
        if phi == 0:
            return 1
        return omega * (1 - phi)

    @staticmethod
    def carn_kill_prob(phi_carn, phi_herb, DeltaPhiMax):
        """
        Find the chance of a carnivore to kill a species

        :param phi_carn: fitness of the carnivore
        :param phi_herb: fitness of the herbivore
        :param DeltaPhiMax: DeltaPhiMax param of the carnivore
        :return: the probability of a sucess hunt
        """
        if phi_carn < phi_herb:
            return 0
        elif phi_carn - phi_herb < DeltaPhiMax:
            return (phi_carn - phi_herb) / DeltaPhiMax
        else:
            return 1

    @staticmethod
    def migrate_prob(mu, phi):
        """
        Find the probability of an animal to move

        :param mu: mu param of the species
        :param phi: fitness of the animal
        :return: probability to move
        """
        if phi > 1 or phi < 0:
            raise ValueError('Phi must be in range [0, 1]')
        return mu * phi

    @staticmethod
    def gain_weight(beta, food):
        """
        Find the weight gains after an amount of food absorption

        :param beta: beta param of the species
        :param food: the amount of food taken
        :return: the weight that gains
        """
        return beta * food

    @staticmethod
    def lose_weight(eta, weight):
        """
        Find the weight loss every year

        :param eta: eta param of the species
        :param weight: current weight of the species
        :return: the weight that loses
        """
        return eta * weight

    @staticmethod
    def birth_weight(w_birth, sigma_birth):
        """
        The weight at birth of an animal is draw from a normal distribution

        :param w_birth: the mean of the normal distribution
        :param sigma_birth: the sigma of the normal distribution
        :return: a weight at birth of the animal
        """
        return rd.gauss(w_birth, sigma_birth)
