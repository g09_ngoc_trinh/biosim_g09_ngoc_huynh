# -*- coding: utf-8 -*-

"""
Definition of all landscapes
"""


__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


class Landscape:
    """
    The base class for all landscapes
    """

    _params = {'f_max': 0}
    color_code = (0.0, 0.0, 0.0)

    @classmethod
    def set_params(cls, params):
        """
        Set the parameters of the landscape

        :param params: dict specifying param to be updated
        """
        cls._params.update(params)

    @classmethod
    def get_params(cls):
        """
        Get the parameters of the landscape

        :return: Dict specifying all parameters of landscape
        """
        return cls._params

    def __init__(self):
        self._fodder = self._params['f_max']
        self.animals = []

    def eaten(self, amount):
        """
        An herbivore tries to take an amount of fodder in the landscape.
        The fodder in the landscape will decrease

        :param amount: the amount of fodder the herbivore wants to take
        :return: the amount of fodder the herbivore can take
        """
        if self._fodder >= amount:
            self._fodder -= amount
            return amount
        else:
            actual_amount = self._fodder
            self._fodder = 0
            return actual_amount

    def grow(self):
        """
        Every year new fodder grows
        """
        raise NotImplementedError("The grow action of this landscape has not been defined")


class Jungle(Landscape):
    """ Inherit from Landscape. Have large amount of fodder"""
    _params = {
        'f_max': 800
    }
    color_code = (0.0, 0.6, 0.0)

    def grow(self):
        """
        Every year a fixed amount of fodder is available
        """
        self._fodder = self._params['f_max']


class Savannah(Landscape):
    """ Inherit from Landscape. Have small amount of fodder. Lose fodder over years"""
    _params = {
        'f_max': 300,
        'alpha': 0.3
    }

    color_code = (0.5, 1.0, 0.5)

    def grow(self):
        """
        Every year new fodder grows an amount of
        `alpha * (f_max - F)`
        """
        self._fodder += self._params['alpha'] * (self._params['f_max'] - self._fodder)


class Ocean(Landscape):
    """ Inherit from landscape."""
    color_code = (0.0, 0.0, 1.0)


class Mountain(Landscape):
    """ Inherit from landscape."""
    color_code = (0.5, 0.5, 0.5)


class Desert(Landscape):
    """ Inherit from landscape."""
    color_code = (1.0, 1.0, 0.5)
