# -*- coding: utf-8 -*-

"""
Definition of all animals live in the area
"""

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

from .formula import Formula
import random as rd
from operator import attrgetter


class Animal:
    """
    The base class for all species
    This class contains all similar method used by other species
    """

    """ 
    A dictionary contains all parameters of the animal, includes:
    - w_birth
    - sigma_birth
    - beta
    - eta
    - a_half
    - phi_age
    - w_half
    - phi_weight
    - mu
    - gamma
    - zeta
    - xi
    - omega
    - F
    - DeltaPhiMax
    """
    _params = {}

    @classmethod
    def set_params(cls, params):
        """
        set the parameters of the animal then force recalculate phi

        :param params: A dictionary of parameters
        """
        cls._params.update(params)

        cls._fitness = None

    @classmethod
    def get_params(cls):
        """
        get the parameters of the animal

        :return _params: dictionary of animal params
        """
        return cls._params

    def __init__(self, age=0, weight=None):
        """
        Create an animal instance with initial age and weight

        :param age: int
        :param weight: float
        """
        self._age = age
        if weight is None:
            if not self._check_param_exist('w_birth', 'sigma_birth'):
                raise ValueError('Missing parameters to calculate this value')
            self._weight = Formula.birth_weight(self._params['w_birth'],
                                                self._params['sigma_birth'])
        else:
            self._weight = weight

        self._fitness = None

        # Flag for attempt to migrate of the animal in a year
        self._migrated = False

    def eat(self, food):
        raise NotImplementedError("The animal does not have this action")

    def birth(self, current_no):
        """
        Check if this animal will give birth to new generation or not

        :param current_no: the number of same kind in the same area
        :return: A new born animal of the same kind
        """
        if not self._check_param_exist('gamma', 'zeta', 'w_birth', 'sigma_birth', 'xi'):
            raise ValueError('Missing parameters to calculate this value')

        if self.weight < self._params['zeta'] \
                * (self._params['w_birth'] + self._params['sigma_birth']):
            return None

        birth_prob = Formula.birth_prob(self._params['gamma'], self.fitness, current_no)

        if rd.random() <= birth_prob:
            new_baby = self.__class__()
            if new_baby.weight >= 0:
                return self.labour(new_baby)

        return None

    def aging(self):
        """ The animal gets older every year. Migrated flag is reset"""
        self.age += 1
        self._migrated = False

    def lose_weight(self):
        """ The animal loses its weight every year """
        self.weight -= Formula.lose_weight(self._params['eta'], self.weight)

    def labour(self, new_baby):
        """
        When an animal gives birth, it will lose weight.
        If baby weight is more than the animal, no babies return.

        :param new_baby: animal of the same kind that it will give birth
        :return: new-born animal of the same type
        """
        if self.weight > new_baby.weight:
            self.weight -= self._params['xi'] * new_baby.weight
            return new_baby
        return None

    @property
    def age(self):
        """ Get the current age of the animal"""
        return self._age

    @age.setter
    def age(self, val):
        """ Set the age of the animal. Reset fitness"""
        self._age = val
        self._fitness = None

    @property
    def weight(self):
        """ Get the current weight of the animal"""
        return self._weight

    @weight.setter
    def weight(self, val):
        """ Set the weight of the animal. Reset finess"""
        self._weight = max(val, 0)
        self._fitness = None

    @property
    def fitness(self):
        """
        Calculate the current fitness based on age and weight.
        Fitness cannot be changed manually.
        """
        if self._fitness is None:
            if not self._check_param_exist('a_half', 'phi_age', 'w_half', 'phi_weight'):
                raise ValueError('Missing parameters to calculate this value')

            params = [
                self.age,
                self._params['a_half'],
                self._params['phi_age'],
                self.weight,
                self._params['w_half'],
                self._params['phi_weight']
            ]

            self._fitness = Formula.phi(*params)

        return self._fitness

    @property
    def migrate(self):
        """
        Check if this animal will migrate or not

        :return: True if the animal can migrate
        """
        if self._migrated:
            return False

        self._migrated = True

        if not self._check_param_exist('mu'):
            raise ValueError('Missing parameters to calculate this value')

        migrate_prob = Formula.migrate_prob(self._params['mu'], self.fitness)
        return rd.random() <= migrate_prob

    @property
    def die(self):
        """
        Check if the animal will die or not

        :return: True if the animal will die
        """
        if not self._check_param_exist('omega'):
            raise ValueError('Missing parameters to calculate this value')

        death_prob = Formula.death_prob(self._params['omega'], self.fitness)
        return rd.random() <= death_prob

    def _check_param_exist(self, *params):
        """
        Check for existence of params.
        eg: self._check_param_exist('param_0', 'param_1')
            will check if both 'param_0' and 'param_1' exist

        :param params: argument list where each item is a param name
        :return: True if all params exists
        """
        for param_name in params:
            if param_name not in self._params.keys():
                return False

        return True


class Herbivore(Animal):
    """ Inherit from Animal. Live on fodder of landscape"""
    _params = {
        'w_birth': 8.0,
        'sigma_birth': 1.5,
        'beta': 0.9,
        'eta': 0.05,
        'a_half': 40.0,
        'phi_age': 0.2,
        'w_half': 10.0,
        'phi_weight': 0.1,
        'mu': 0.25,
        'gamma': 0.2,
        'zeta': 3.5,
        'xi': 1.2,
        'omega': 0.4,
        'F': 10
    }

    def eat(self, landscape):
        """
        The herbivore will eat the fodder inside the landscape and gain weight based on the
        fodder taken

        :param landscape
        """
        if not self._check_param_exist('beta', 'F'):
            raise ValueError('Missing parameters to calculate this value')

        eat_amount = landscape.eaten(self._params['F'])
        self.weight += Formula.gain_weight(self._params['beta'], eat_amount)


class Carnivore(Animal):
    """ Inherit from Animal. Live on Herbivore in living location """
    _params = {
        'w_birth': 6.0,
        'sigma_birth': 1.0,
        'beta': 0.75,
        'eta': 0.125,
        'a_half': 60.0,
        'phi_age': 0.4,
        'w_half': 4.0,
        'phi_weight': 0.4,
        'mu': 0.4,
        'gamma': 0.8,
        'zeta': 3.5,
        'xi': 1.1,
        'omega': 0.9,
        'F': 50.0,
        'DeltaPhiMax': 10.0
    }

    def eat(self, herbivores, is_sorted=False):
        """
        The carnivore will hunt for herbivore start from the one with lowest fitness.
        If the hunt is success it will gain a weight based the weight of the herbivore

        :param herbivores: a list of herbivores for the carnivore to hunt
        :param is_sorted: implies if the herbivore list has been sorted or not
        :return: list of survived herbivores
        """
        if not self._check_param_exist('beta', 'F'):
            raise ValueError('Missing parameters to calculate this value')

        if not is_sorted:
            sorted(herbivores, key=attrgetter('fitness'))

        absorbed = 0
        for index, herb in enumerate(herbivores):
            if absorbed >= self._params['F']:
                break
            if self.hunt(herb):
                absorbed += Formula.gain_weight(self._params['beta'], herb.weight)
                herbivores[index] = None

        self.weight += absorbed

        return [herb for herb in herbivores if herb is not None]

    def hunt(self, herbivore):
        """
        Based on the fitness of the Carnivore and the Herbivore to determine the result of the hunt

        :param herbivore: the herbivore to be hunted
        :return: True if the hunt success
        """
        if not self._check_param_exist('DeltaPhiMax'):
            raise ValueError('Missing parameters to calculate this value')

        return rd.random() <= \
            Formula.carn_kill_prob(self.fitness, herbivore.fitness, self._params['DeltaPhiMax'])
