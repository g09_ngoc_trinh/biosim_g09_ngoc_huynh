# -*- coding: utf-8 -*-

"""
Definition of the island map
"""

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


from .landscape import Jungle, Savannah, Mountain, Ocean, Desert
import numpy as np


class Map:
    """
    A Map contains a 2-D arrays of landscape where position (0,0) is the top-left coord.
    Each cell/point is an instance of Landscape.
    """
    _landscapes = {
        'J': Jungle,
        'S': Savannah,
        'O': Ocean,
        'M': Mountain,
        'D': Desert
    }

    def __init__(self, island_map):
        """
        Initialize the island map

        :param island_map: a multi-line string, each character is a code-key represents a landscape
        """
        isl_map = island_map.strip().splitlines()
        isl_map = np.array([list(landscape) for landscape in isl_map])

        shape = isl_map.shape
        if len(shape) < 2:
            raise ValueError('Map has inconsistent length')

        if not (np.all(isl_map[:, 0] == isl_map[:, shape[1] - 1]) and
                np.all(isl_map[:, 0] == np.array(['O' for _ in range(shape[0])])) and
                np.all(isl_map[0, :] == isl_map[shape[0] - 1, :]) and
                np.all(isl_map[0, :] == np.array(['O' for _ in range(shape[1])]))):
            raise ValueError('Map has invalid boundary')

        self._island_map = np.empty(shape, dtype=object)

        for i in range(shape[0]):
            for j in range(shape[1]):
                if not isl_map[i, j] in self._landscapes.keys():
                    raise ValueError('Invalid landscape')
                self._island_map[i, j] = self._landscapes[isl_map[i, j]]()

        self._color_map = None

    def __getitem__(self, key):
        """
        Magic function to return item at a tuple-like coord
        `map[a,b]` will return item at [a,b] coord
        """
        return self._island_map.__getitem__(key)

    def __setitem__(self, key, value):
        """
        Magic function to set item at a tuple-like coord
        `map[a,b] = lscape` will set item at `[a,b]` coord into `lscape`
        """
        return self._island_map.__setitem__(key, value)

    def set_params(self, landscape, params):
        """
        Set parameters for landscape type.

        :param landscape: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """
        self._landscapes[landscape].set_params(params)

    @property
    def shape(self):
        """Return the shape (row, col) of the island"""
        return self._island_map.shape

    @property
    def color_map(self):
        """Return the image of the island based on the color_code of each landscape"""
        if self._color_map is None:
            self._color_map = [[cell.color_code for cell in row] for row in self._island_map]

        return self._color_map
