# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


import textwrap
from biosim.simulation import BioSim
from examples.population_generator import Population
import matplotlib.pyplot as plt

if __name__ == '__main__':
    geogr = """\
               OOOOOOOOO
               OJJJJJJJO
               OJJJJJJJO
               OJJJJJJJO
               OJJJJJJJO
               OJJJJJJJO
               OJJJJJJJO
               OJJJJJJJO
               OOOOOOOOO"""
    geogr = textwrap.dedent(geogr)
    # plt.ion()
    sim = BioSim(island_map=geogr,
                 ini_pop=[],
                 seed=487539,
                 cmax_animals={'Herbivore': 200, 'Carnivore': 50},
                 img_base='./tmp/migrate_img'
                 )
    sim.set_animal_parameters('Herbivore',
                              {'mu': 1, 'omega': 0, 'gamma': 0,
                               'a_half': 1000})
    sim.set_animal_parameters('Carnivore',
                              {'mu': 1, 'omega': 0, 'gamma': 0,
                               'F': 0, 'a_half': 1000})
    sim.add_population([
        {'loc': (5, 5), 'pop': [{'species': 'Herbivore', 'age': 5, 'weight': 50}
                                for _ in range(1000)]},
        {'loc': (5, 5), 'pop': [{'species': 'Carnivore', 'age': 5, 'weight': 50}
                                for _ in range(1000)]}
    ])

    sim.simulate(50, vis_years=1, img_years=1)
    plt.show()
    sim.make_movie()
