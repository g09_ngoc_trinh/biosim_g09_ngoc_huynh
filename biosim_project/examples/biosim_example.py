# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

import textwrap
from biosim.simulation import BioSim

if __name__ == '__main__':

    geogr = """\
                   OOOOOOOOOOOOOOOOOOOO
                   OOOOOOOSMMMMJJSSSSSO
                   OOOSSJJJJMMJSSSSSSOO
                   OOOSSSSSSMMJJJSSSOOO
                   OOSSSJJSSSSSJJSSSOOO
                   OSSSSJJJDDJJJSSSJOOO
                   OSJJJJJDDDJSSSSSSOOO
                   OSSSSSSJDDJJSSOOOOOO
                   OSSSSJJJDDJJJJSSSOOO
                   OOOSSSJJDDJJSSOOOOOO
                   OOOOOSSJSSSSSOOOOOOO
                   OOOOOSSSSSSSSOOOOOOO
                   OOOOOOOOOOOOOOOOOOOO"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (10, 8),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(150)]}]

    ini_carns = [{'loc': (10, 8),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(50)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                 seed=848484, cmax_animals={'Herbivore': 150, 'Carnivore': 100}, img_base='./tmp/biosim')

    sim.set_animal_parameters('Herbivore', {'zeta': 3.2, 'xi': 1.8})
    sim.set_animal_parameters('Carnivore', {'a_half': 70, 'phi_age': 0.5,
                                            'omega': 0.3, 'F': 65,
                                            'DeltaPhiMax': 9.})

    sim.set_landscape_parameters('J', {'f_max': 700})
    sim.set_landscape_parameters('S', {'f_max': 200, 'alpha': 0.2})

    sim.simulate(num_years=100, vis_years=1, img_years=1)
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=500, vis_years=1, img_years=1)

    input('Press ENTER to create Movies')
    sim.make_movie()
