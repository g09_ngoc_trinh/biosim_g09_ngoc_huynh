# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

from biosim.simulation import BioSim
from examples.population_generator import Population
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # plt.ion()
    sim = BioSim(island_map="OOO\nOJO\nOOO",
                     ini_pop=[],
                     seed=889888)
    sim.add_population(Population(50, [(2,2)], None, None).get_animals())
    sim.simulate(50, vis_years=1, img_years=1)
    sim.add_population(Population(None, None, 5, [(2,2)]).get_animals())
    sim.simulate(400, vis_years=1, img_years=1)
    plt.show()
