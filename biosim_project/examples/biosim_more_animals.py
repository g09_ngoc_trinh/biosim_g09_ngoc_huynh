# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

import textwrap
import matplotlib.pyplot as plt
import pandas as pd
from operator import attrgetter

from biosim.simulation import BioSim
from biosim.animal import Animal, Herbivore, Carnivore


class Bird(Herbivore):
    _params = {
        'w_birth': 3.0,
        'sigma_birth': 1.5,
        'beta': 0.9,
        'eta': 0.05,
        'a_half': 40.0,
        'phi_age': 0.2,
        'w_half': 4.0,
        'phi_weight': 0.1,
        'mu': 0.9,
        'gamma': 0.2,
        'zeta': 3.5,
        'xi': 1.2,
        'omega': 0.4,
        'F': 4
    }


class BioSimExtend(BioSim):
    _species_name = {
        'Herbivore': Herbivore,
        'Carnivore': Carnivore,
        'Bird': Bird
    }

    @property
    def animal_distribution(self):
        """Pandas DataFrame with animal count per species for each cell on island."""
        population = self.population
        shape = population.shape

        data = [[i + 1, j + 1,
                 len(population[i, j]['Herbivore']),
                 len(population[i, j]['Carnivore']),
                 len(population[i, j]['Bird'])]
                for i in range(shape[0]) for j in range(shape[1])]

        return pd.DataFrame(data, columns=['Row', 'Col', 'Herbivore', 'Carnivore', 'Bird'])

    def _feeding(self, pop, landscape):
        bird = pop['Bird']

        sorted(bird, key=attrgetter('fitness'), reverse=True)
        for b in bird:
            b.eat(landscape)

        super()._feeding(pop, landscape)


if __name__ == '__main__':
    plt.rcParams['figure.figsize'] = (12, 6)

    geogr = """\
                   OOOOOOOOOOOOOOOOOOOO
                   OOOOOOOSMMMMJJSSSSSO
                   OOOSSJJJJMMJSSSSSSOO
                   OOOSSSSSSMMJJJSSSOOO
                   OOSSSJJSSSSSJJSSSOOO
                   OSSSSJJJDDJJJSSSJOOO
                   OSJJJJJDDDJSSSSSSOOO
                   OSSSSSSJDDJJSSOOOOOO
                   OSSSSJJJDDJJJJSSSOOO
                   OOOSSSJJDDJJSSOOOOOO
                   OOOOOSSJSSSSSOOOOOOO
                   OOOOOSSSSSSSSOOOOOOO
                   OOOOOOOOOOOOOOOOOOOO"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (10, 8),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(150)]}]

    init_birds = [{'loc': (10, 8),
                  'pop': [{'species': 'Bird',
                           'age': 2,
                           'weight': 8}
                          for _ in range(150)]},
                  {'loc': (9, 13),
                   'pop': [{'species': 'Bird',
                            'age': 2,
                            'weight': 8}
                           for _ in range(150)]}
                  ]

    ini_carns = [{'loc': (10, 8),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(50)]}]

    sim = BioSimExtend(island_map=geogr, ini_pop=ini_herbs,
                 seed=848484, cmax_animals={'Herbivore': 150, 'Carnivore': 100, 'Bird': 200})

    sim.set_animal_parameters('Herbivore', {'zeta': 3.2, 'xi': 1.8})
    sim.set_animal_parameters('Carnivore', {'a_half': 70, 'phi_age': 0.5,
                                            'omega': 0.3, 'F': 65,
                                            'DeltaPhiMax': 9.})

    sim.set_landscape_parameters('J', {'f_max': 700})
    sim.set_landscape_parameters('S', {'f_max': 200, 'alpha': 0.2})

    sim.simulate(num_years=50, vis_years=1, img_years=1)
    sim.add_population(population=init_birds)
    # sim.simulate(num_years=50, vis_years=1, img_years=1)
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=500, vis_years=1, img_years=1)

    plt.show()
