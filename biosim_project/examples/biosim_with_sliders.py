# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'


import textwrap
import matplotlib.pyplot as plt
from matplotlib.widgets import Button, Slider

from biosim.simulation import BioSim


class BioSimGui:
    def __init__(self):
        plt.rcParams['figure.figsize'] = (10, 8)
        geogr = """\
                       OOOOOOOOOOOOOOOOOOOO
                       OOOOOOOSMMMMJJSSSSSO
                       OOOSSJJJJMMJSSSSSSOO
                       OOOSSSSSSMMJJJSSSOOO
                       OOSSSJJSSSSSJJSSSOOO
                       OSSSSJJJDDJJJSSSJOOO
                       OSJJJJJDDDJSSSSSSOOO
                       OSSSSSSJDDJJSSOOOOOO
                       OSSSSJJJDDJJJJSSSOOO
                       OOOSSSJJDDJJSSOOOOOO
                       OOOOOSSJSSSSSOOOOOOO
                       OOOOOSSSSSSSSOOOOOOO
                       OOOOOOOOOOOOOOOOOOOO"""
        geogr = textwrap.dedent(geogr)

        ini_herbs = [{'loc': (11, 10),
                      'pop': [{'species': 'Herbivore',
                               'age': 5,
                               'weight': 20}
                              for _ in range(50)]}]

        sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                     seed=848484)

        sim.set_animal_parameters('Herbivore', {'zeta': 3.2, 'xi': 1.8})
        sim.set_animal_parameters('Carnivore', {'a_half': 70, 'phi_age': 0.5,
                                                'omega': 0.3, 'F': 65,
                                                'DeltaPhiMax': 9.})

        sim.set_landscape_parameters('J', {'f_max': 700})
        sim.set_landscape_parameters('S', {'f_max': 200, 'alpha': 0.2})

        figure = sim.visual_panel.figure

        ax_herb_gamma = figure.add_axes([0.1, 0.06, 0.1, 0.02])
        ax_herb_omega = figure.add_axes([0.1, 0.03, 0.1, 0.02])
        ax_carn_gamma = figure.add_axes([0.33, 0.06, 0.1, 0.02])
        ax_carn_omega = figure.add_axes([0.33, 0.03, 0.1, 0.02])
        ax_simulate = figure.add_axes([0.49, 0.03, 0.25, 0.05])
        ax_pause = figure.add_axes([0.76, 0.03, 0.1, 0.05])
        ax_quit = figure.add_axes([0.88, 0.03, 0.1, 0.05])

        self.fig = figure
        self.sim = sim

        self.sld_herb_gamma = Slider(ax_herb_gamma,
                               label='Herbivore $\gamma$',
                               valmin=0, valmax=1,
                               valinit=0.2,
                               valstep=0.1,
                               valfmt='%.1f')
        self.sld_herb_gamma.on_changed(self._set_gamma_herb)

        self.sld_herb_omega = Slider(ax_herb_omega,
                                     label='Herbivore $\omega$',
                                     valmin=0, valmax=1,
                                     valinit=0.4,
                                     valstep=0.1,
                                     valfmt='%.1f')
        self.sld_herb_omega.on_changed(self._set_omega_herb)

        self.sld_carn_gamma = Slider(ax_carn_gamma,
                                     label='Carnivore $\gamma$',
                                     valmin=0, valmax=1,
                                     valinit=0.8,
                                     valstep=0.1,
                                     valfmt='%.1f')
        self.sld_carn_gamma.on_changed(self._set_gamma_carn)

        self.sld_carn_omega = Slider(ax_carn_omega,
                                     label='Carnivore $\omega$',
                                     valmin=0, valmax=1,
                                     valinit=0.9,
                                     valstep=0.1,
                                     valfmt='%.1f')
        self.sld_carn_omega.on_changed(self._set_omega_carn)

        self.btn_sim = Button(ax_simulate, 'Simulate 100 years more')
        self.btn_sim.on_clicked(self.simulate)
        self.btn_sim.set_active(False)

        self.btn_pause = Button(ax_pause, 'Pause')
        self.btn_pause.on_clicked(self.pause)
        self.btn_pause.set_active(False)

        self.btn_quit = Button(ax_quit, 'Quit')
        self.btn_quit.on_clicked(self.close)

    def _set_omega_carn(self, value):
        sim = self.sim
        sim.set_animal_parameters('Carnivore', {'omega': value})

    def _set_gamma_carn(self, value):
        sim = self.sim
        sim.set_animal_parameters('Carnivore', {'gamma': value})

    def _set_omega_herb(self, value):
        sim = self.sim
        sim.set_animal_parameters('Herbivore', {'omega': value})

    def _set_gamma_herb(self, value):
        sim = self.sim
        sim.set_animal_parameters('Herbivore', {'gamma': value})

    def add_carn(self):
        sim = self.sim
        sim.add_population([{'loc': (11, 11),
                             'pop': [{'species': 'Carnivore',
                                      'age': 5,
                                      'weight': 20} for _ in range(50)]}])

    def pause(self, evt):
        sim = self.sim
        sim.pause()
        self.btn_sim.set_active(True)

    def simulate(self, evt):
        sim = self.sim
        self.btn_sim.set_active(False)
        self.btn_pause.set_active(True)
        sim.simulate(num_years=100, vis_years=1, img_years=2000)
        self.btn_sim.set_active(True)

    def show(self):
        sim = self.sim
        sim.simulate(num_years=100, vis_years=1, img_years=2000)
        sim.add_population([{'loc': (11, 11),
                             'pop': [{'species': 'Carnivore',
                                      'age': 5,
                                      'weight': 20} for _ in range(50)]}])
        self.btn_sim.set_active(True)
        self.btn_pause.set_active(True)
        plt.show(self.fig)

    def close(self, evt):
        self.sim.pause()
        plt.close(self.fig)


if __name__ == '__main__':
    gui = BioSimGui()
    gui.show()
