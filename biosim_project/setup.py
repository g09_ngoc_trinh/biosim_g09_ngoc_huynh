# -*- coding: utf-8 -*-

__author__ = 'Ngoc Huynh'
__email__ = 'ngochuyn@nmbu.no'

"""
To create biosim package, first run this command in python termimal at the directory of this file
> python setup.py sdist

To install the biosim package, first move the package file to destination directory, unpack it,
then run
> python setup.py install
"""

from distutils.core import setup

setup(name='BioSim',
      version='1.0',
      description='BioSim',
      author='Ngoc Huynh',
      author_email='ngochuyn@nmbu.no',
      url='https://bitbucket.org/g09_ngoc_trinh/biosim_g09_ngoc_huynh',
      requires=['numpy', 'pandas', 'matplotlib'],
      packages=['biosim', 'biosim.tests']
      )
