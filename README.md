# Bio Sim Project

## INF200 January Project

### Team Member
- Ngoc Huynh ngochuyn@nmbu.no
- ~~Huynh Nhat Trinh Phan huph@nmbu.no~~

### About
The biosim package provides modules to create a Python application to simulate the existence and movement of animals on a designated island.

### Setup and Install
In biosim_project directory, run

`> python setup.py install`

The biosim package will be created inside `/dist` directory.

To use the biosim package, move it to the destination folder, unpack it and run either:

```
> python setup.py install
> python setup.py install --user
> python setup.py install --prefix=/Users/test
```

### Examples
Inside `/examples directory, there are examples of the biosim package.

- `/examples/checksim.py` checks for compatibility of the biosim package
- `/examples/simulation_one_cell.py`: run the simulation of animals in one area without migration
- `/examples/simulation_with_migration.py`: run the simulation of animals in a unified-landscape island without dying (including the cause of hunting) and procreation. Also creating movies example
- `/examples/biosim_example.py`: run a basic GUI simulation of an island with all landscape and create a mp4 file after the simulation
- `/examples/biosim_with_widget.py`: run a customized GUI simulation of an island with all landscape. The customized GUI includes widgets such as buttons to stop / start simulation
- `/examples/population_generator.py` a helper class created by Ragnhild Smistad, UMB and Toril Fjeldaas Rygg, UMB, used for generate random animal population on the island
